import { extraRunsPerTeam } from "../src/server/functions/extraRunsPerTeam.js";
import { csv2json } from "../src/server/functions/csv2jsonConvereter.js";

var subMatchesJson;
var subDeliveriesJson;

try {
  subMatchesJson = csv2json("src/data/testData/subMatches.csv");
  subDeliveriesJson = csv2json("src/data/testData/subDeliveries.csv");
} catch (e) {
  console.log(e.message);
}

test("extraRunsPerTeam function throws error when null is passed as matchesJson", () => {
  expect(() => extraRunsPerTeam(null, subDeliveriesJson, "2016")).toThrow();
});

test("extraRunsPerTeam function throws errors when undefined is passed as matchesJson", () => {
  expect(() =>
    extraRunsPerTeam(undefined, subDeliveriesJson, "2016")
  ).toThrow();
});

test("extraRunsPerTeam function throws error when null is passed as subDeliveriesJson", () => {
  expect(() => extraRunsPerTeam(subMatchesJson, null, "2016")).toThrow();
});

test("extraRunsPerTeam function throws errors when undefined is passed as subdeliveriesJson", () => {
  expect(() => extraRunsPerTeam(subMatchesJson, undefined, "2016")).toThrow();
});
test("extraRunsPerTeam function throws errors when null is passed as year", () => {
  expect(() => extraRunsPerTeam(subMatchesJson, undefined, null)).toThrow();
});
test("extraRunsPerTeam function throws errors when undefined is passed as year", () => {
  expect(() => extraRunsPerTeam(subMatchesJson, undefined, undefined)).toThrow();
});

test('providing testfiles result is {"Royal Challengers Bangalore":4} in year 2017', () => {
  expect(extraRunsPerTeam(subMatchesJson, subDeliveriesJson, "2017")).toEqual(
    expect.objectContaining({ "Royal Challengers Bangalore": 4 })
  );
});
