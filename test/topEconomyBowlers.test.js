import { topEconomyBowlers } from "../src/server/functions/topEconomyBowlers.js";
import { csv2json } from "../src/server/functions/csv2jsonConvereter.js";

var subMatchesJson;
var subDeliveriesJson;

try {
  subMatchesJson = csv2json("src/data/testData/subMatches.csv");
  subDeliveriesJson = csv2json("src/data/testData/subDeliveries.csv");
} catch (e) {
  console.log(e.message);
}

test("topEconomyBowlers function throws error when null is passed as matchesJson", () => {
  expect(() => topEconomyBowlers(null, subDeliveriesJson,"2015")).toThrow();
});

test("topEconomyBowlers function throws errors when undefined is passed as matchesJson", () => {
  expect(
    () => topEconomyBowlers(undefined, subDeliveriesJson,"2015")
  ).toThrow();
});

test("topEconomyBowlers function throws error when null is passed as subDeliveriesJson", () => {
  expect(() => topEconomyBowlers(subMatchesJson, null, "2015")).toThrow();
});

test("topEconomyBowlers function throws errors when undefined is passed as subdeliveriesJson", () => {
  expect(() => topEconomyBowlers(subMatchesJson, undefined, "2015")).toThrow();
});

test("topEconomyBowlers function throws error when null is passed as year", () => {
  expect(() =>
    topEconomyBowlers(subMatchesJson, subDeliveriesJson, null)
  ).toThrow();
});

test("topEconomyBowlers function throws errors when undefined is passed as year", () => {
  expect(() =>
    topEconomyBowlers(subMatchesJson, subDeliveriesJson, undefined)
  ).toThrow();
});

test("economy functions on providing testfiles has TS Mills with economy 6 on passing year as 2017", () => {
  expect(topEconomyBowlers(subMatchesJson, subDeliveriesJson, "2017")).toEqual(
    expect.arrayContaining([["TS Mills", 7]])
  );
});
