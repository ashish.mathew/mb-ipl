import { matchesPerYear } from '../src/server/functions/matchesPerYear.js'
import { csv2json } from '../src/server/functions/csv2jsonConvereter.js';

var subMatchesJson;

try {
  subMatchesJson = csv2json(
    "src/data/testData/subMatches.csv"
  );
} catch (e) {
  console.log(e.message);
}

test("matchesPerYear function throws error when passing null", () => {
  expect(() => matchesPerYear(null)).toThrow();
});

test("matchesPerYear function throws error when passing undefined", () => {
  expect(() => matchesPerYear(undefined)).toThrow()
});

test('matchesperyear function has 2017 has  3',() => {
    expect(matchesPerYear(subMatchesJson)).toMatchObject({"2017" : 3})
})
