import { matchesWonPerTeamPerYear } from "../src/server/functions/matchesWonPerTeamPerYear.js";
import { csv2json } from "../src/server/functions/csv2jsonConvereter.js";


var subMatchesJson;


try {
  subMatchesJson = csv2json(
    "src/data/testData/subMatches.csv"
  );
} catch (e) {
  console.log(e.message);
}

test("matchesWonPerTeamPerYear function throws error when passing null", () => {
  expect(() => matchesWonPerTeamPerYear(null)).toThrow();
});

test("matchesWonPerTeamPerYear function throws error when passing undefined", () => {
  expect(() => matchesWonPerTeamPerYear(undefined)).toThrow();
});

test("matchesWonPerTeamPerYear function throws error when passing empty object", () => {
  expect(() => matchesWonPerTeamPerYear({})).toThrow();
});

test('matchesWonPerteamPerYear function has "Sunrisers Hyderabad":1 on year 2017', () => {
  expect(matchesWonPerTeamPerYear(subMatchesJson)["2017"]).toMatchObject({"Sunrisers Hyderabad":1})
});
