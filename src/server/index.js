import fs from "fs";
import { matchesPerYear } from "./functions/matchesPerYear.js";
import { matchesWonPerTeamPerYear } from "./functions/matchesWonPerTeamPerYear.js";
import { extraRunsPerTeam } from "./functions/extraRunsPerTeam.js";
import { topEconomyBowlers } from "./functions/topEconomyBowlers.js";
import { csv2json } from "./functions/csv2jsonConvereter.js";
import { superBowlerEconomy } from "./functions/bestEconomyInsuperOver.js";

var matchesJson;
var deliveriesJson;

try {
  matchesJson = csv2json(
    "../../src/data/matches.csv"
  );
  deliveriesJson = csv2json(
    "../../src/data/deliveries.csv"
  );
} catch (e) {
  console.log(e.message);
}


try {
  const matchesPerYearResult = matchesPerYear(matchesJson);
  fs.writeFileSync(
    "../public/output/matchesPerYear.json",
    JSON.stringify(matchesPerYearResult)
  );
} catch (e) {
  console.log(e.message);
}

try{
  const matchesWonPerTeamPerYearResult = matchesWonPerTeamPerYear(matchesJson);
fs.writeFileSync(
  "../public/output/matchesWonPerTeamPerYear.json",
  JSON.stringify(matchesWonPerTeamPerYearResult)
);
}catch(e){
  console.log(e.message);
}

try{
  const extraRunsPerTeamResult = extraRunsPerTeam(matchesJson,deliveriesJson, "2016");
fs.writeFileSync(
  "../public/output/extraRunsPerTeam.json",
  JSON.stringify(extraRunsPerTeamResult)
);
}catch(e){
  console.log(e.message);
}

try{
  const topEconomyBowlersResult = topEconomyBowlers(matchesJson,deliveriesJson, "2015");
fs.writeFileSync(
  "../public/output/topEconomyBowlers.json",
  JSON.stringify(topEconomyBowlersResult)
);
}catch(e){
  console.log(e.message);
}



try{
  const topEconomicalBowlerInSuperOverResult = superBowlerEconomy(deliveriesJson)
  fs.writeFileSync(
    "../public/output/bestEconomyInSuperOver.json",
    JSON.stringify(topEconomicalBowlerInSuperOverResult)
  );
}catch(e){
  console.log(e.message);
}
// fs.writeFileSync(
//   "../public/wonTossAndWonMatch.json",
//   JSON.stringify(won_toss_won_match())
// );
// fs.writeFileSync(
//   "../public/highest dismissal a player by another.json",
//   JSON.stringify(highest_dismissal())
// );

// fs.writeFileSync(
//   "../public/highest_no_mvp_per_season.json",
//   JSON.stringify(mom())
// );
