//functions returns no of matches played per year
import { isInvalid } from "./helperFunction.js";
export function matchesPerYear(matchesJson) {
  if (isInvalid(matchesJson)) {
    throw new Error(`${matchesJson} is an invalid data`);
  }

  return matchesJson.reduce((matchesCount, currentMatch) => {
    let year = currentMatch.season;
    matchesCount[year] = ++matchesCount[year] || 1;
    return matchesCount;
  }, {});
}
