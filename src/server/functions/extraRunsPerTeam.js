//function returns extra runs conceded per team in the year 2016
import { findMatchIds, isInvalid } from "./helperFunction.js";
export function extraRunsPerTeam(matchesJson, deliveriesJson, year) {
  if (isInvalid(matchesJson))
    throw new Error(`${matchesJson} is an invalid data`);
  if (isInvalid(deliveriesJson))
    throw new Error(`${deliveriesJson} is an invalid data`);

  if (typeof year !== "string") throw new Error(`${year} is invalid data`);

  let matchIds = [];
  matchIds = findMatchIds(matchesJson, year);

  return deliveriesJson.reduce((obj, delivery) => {
    if (matchIds.includes(delivery.match_id) && delivery.extra_runs >= 1) {
      let team = delivery.bowling_team;
      let extraRun = parseInt(delivery.extra_runs);
      obj[team] = obj[team] + extraRun || extraRun;
    }
    return obj;
  }, {});
}
