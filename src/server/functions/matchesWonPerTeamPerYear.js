import { isInvalid } from "./helperFunction.js";

//function returns no of matches won per team per year
export function matchesWonPerTeamPerYear(matchesJson) {
  if (isInvalid(matchesJson))
    throw new Error(`${matchesJson} is an invalid data`);

  return matchesJson.reduce((obj, match) => {
    let { season, winner } = match;
    if (typeof obj[season] === "undefined") obj[season] = {};
    obj[season][winner] = ++obj[season][winner] || 1;
    return obj;
  }, {});
}
