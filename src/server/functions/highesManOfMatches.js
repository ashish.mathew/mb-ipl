//highest No Of Man Of Matches
export function manOfMatch() {
    let years = [];
    let manOfMatchWinners = [];
    let highest_mvp = [];
    matchesJson.map((match) => {
      if (!years.includes(match.season)) years.push(match.season);
      return years;
    });
    years.forEach((year) => {
      manOfMatchWinners.push(
        matchesJson.reduce((mvps, match) => {
          if (match.season == year) {
            if (mvps[match.player_of_match]) {
              mvps[match.player_of_match]++;
            } else {
              mvps["year"] = year;
              mvps[match.player_of_match] = 1;
            }
          }
          return mvps;
        }, {})
      );
    });
    manOfMatchWinners.forEach((winner) =>
      highest_mvp.push(
        Object.entries(winner)
          .sort((a, b) => {
            return b[1] - a[1];
          })
          .slice(0, 2)
      )
    );
    return highest_mvp;
  }