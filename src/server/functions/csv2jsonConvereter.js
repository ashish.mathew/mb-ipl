import fs from "fs";
//functions converts csv files to json
export function csv2json(filepath) {
  //rexport ead the file contents and seperate the header and data
  let fileData = fs.readFileSync(filepath, "UTF-8");
  let dataValues = fileData.split("\r\n");
  let [firstvalue, ...restvalues] = dataValues;
  let dataProperties = firstvalue.split(",");
  let arrayOfObj = restvalues.map((value) => {
    return value.split(",").reduce((eachObj, currValue, index) => {
      eachObj[dataProperties[index]] = currValue;
      return eachObj;
    }, {});
  });
  return arrayOfObj;
}
