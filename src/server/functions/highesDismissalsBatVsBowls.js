//highest dismissals of a batsman by a bowler
export function highestDismissal() {
    let batsmans = [];
    let bowlersData = [];
    let dismissals = [];
    let deliveries = deliveriesJson.filter(
      (delivery) => delivery.player_dismissed != ""
    );
  
    deliveries.map((delivery) => {
      if (!batsmans.includes(delivery.batsman)) batsmans.push(delivery.batsman);
      if (!bowlersData.includes(delivery.bowler))
        bowlersData.push(delivery.bowler);
    });
    bowlersData.forEach((bowler) => {
      batsmans.forEach((batsman) => {
        dismissals.push(
          deliveries.reduce((batsVsBowls, delivery) => {
            if (
              delivery.player_dismissed == batsman &&
              delivery.bowler == bowler
            ) {
              if (
                batsVsBowls["batsman"] == batsman &&
                batsVsBowls["bowler"] == bowler
              ) {
                batsVsBowls["count"]++;
              } else {
                batsVsBowls["batsman"] = batsman;
                batsVsBowls["bowler"] = bowler;
                batsVsBowls["count"] = 1;
              }
            }
            return batsVsBowls;
          }, {})
        );
      });
    });
    return dismissals
      .sort((a, b) => {
        return b.count - a.count;
      })
      .slice(0, 1);
  }