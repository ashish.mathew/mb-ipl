//fnds matchids in a year
export function findMatchIds(matchesJson, year) {
  return matchesJson
    .filter((match) => {
      return match.season == year;
    })
    .map((match) => {
      return match.id;
    });
}

export function isInvalid(data) {
  if (
    typeof data === "undefined" ||
    typeof data == null ||
    typeof data !== "object" ||
    Object.entries(data).lenght == 0
  )
    return 1;
  return 0;
}
