//funtions returns top 10 topEconomyBowlersData of year 2015;
import { findMatchIds, isInvalid } from "./helperFunction.js";
export function topEconomyBowlers(matchesJson, deliveriesJson, year) {
  if (isInvalid(matchesJson))
    throw new Error(`${matchesJson} is an invalid data`);
  if (isInvalid(deliveriesJson))
    throw new Error(`${deliveriesJson} is an invalid data`);
  if (typeof year !== "string") throw new Error(`${year} is invalid data`);

  let matchIds = [];
  let bowlersData = [];
  let topBowlers = [];
  matchIds = findMatchIds(matchesJson, year);

  bowlersData = deliveriesJson.reduce((obj, delivery) => {
    if (matchIds.includes(delivery.match_id)) {
      let bowler = delivery.bowler;
      let runs = parseInt(delivery.total_runs);
      let extraruns =
        parseInt(delivery.noball_runs) + parseInt(delivery.wide_runs);
      if (obj.hasOwnProperty(bowler)) {
        obj[bowler]["runs"] += runs;
        obj[bowler]["balls"]++;
      } else {
        obj[bowler] = {};
        obj[bowler]["runs"] = runs;
        obj[bowler]["balls"] = 1;
      }
      if (extraruns > 0) --obj[bowler]["balls"];
    }
    return obj;
  }, {});

  for (let [bowler, data] of Object.entries(bowlersData)) {
    let { runs, balls } = data;
    let economy = runs / (balls / 6);
    topBowlers.push([bowler, economy]);
  }
  return topBowlers
    .sort((a, b) => {
      return a[1] - b[1];
    })
    .slice(0, 10);
}
