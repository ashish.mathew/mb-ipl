//counts no of times teams won toss and won match
export function wonTossWonMatch() {
    let winnings = {};
    return matchesJson.reduce((winnings, match) => {
      if (match.toss_winner == match.winner) {
        winnings[match.winner] = ++winnings[match.winner] || 1;
      }
      return winnings;
    }, winnings);
  }