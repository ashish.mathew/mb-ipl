//best economy bowler in superover
import { isInvalid } from "./helperFunction.js";
export function superBowlerEconomy(deliveriesJson) {
    let bowlers = [];
    let bowlerAndEconomy = [];
    if(isInvalid(deliveriesJson)){
      throw new Error(`${ deliveriesJson} is an invalid data`);
    }
    bowlers = deliveriesJson.reduce((objBowlers,currDelivery) => {
      if(currDelivery.is_super_over > 0){
        let { bowler } = currDelivery;
        let totalRuns = parseInt(currDelivery.total_runs);
        let extraRuns = parseInt(currDelivery.noball_runs) + parseInt(currDelivery.wideball_runs);
        if(objBowlers.hasOwnProperty(bowler)){
          objBowlers[bowler]['totalRuns'] += totalRuns;
          objBowlers[bowler]['totalBalls']++;
        }else{
          objBowlers[bowler] = {};
          objBowlers[bowler]['totalRuns'] = totalRuns;
          objBowlers[bowler]['totalBalls'] = 1;
        }
        if(extraRuns > 0){
          --objBowlers[bowler]['totalBalls'];
        }
      }
      return objBowlers;
    },{});
    for (let [bowler,data] of Object.entries(bowlers)){
      let { totalRuns,totalBalls} = data;
      let economy = totalRuns/(totalBalls/6);
      bowlerAndEconomy.push([bowler,economy]);
    }
    return bowlerAndEconomy.sort((a,b) => {
      return a[1] - b[1];
    }).slice(0,1);
  }